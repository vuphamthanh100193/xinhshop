import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  template: '<app-login></app-login>',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
